#!/bin/bash

if [ `ss -tunlp|grep 7777 |wc -l` -gt 0 ];then
    lsof -i:7777 |awk -F ' ' 'NR>1 {print $2}'|xargs kill -9
fi