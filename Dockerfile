from myhub.mydocker.com/base/python:3.6

copy requirements.txt ./
# RUN pip install --no-cache-dir -r requirements.txt
RUN pip install -r requirements.txt
COPY websocket.py /opt/microservices/
COPY api /opt/microservices/api
COPY conf /opt/microservices/conf
COPY static /opt/microservices/static
COPY templates /opt/microservices/templates
EXPOSE 7777
WORKDIR /opt/microservices

ENTRYPOINT [ "python","websocket.py"]
# ADD cmd.sh /root/
# RUN chmod +x /root/cmd.sh
# CMD ["/root/cmd.sh","arg1"]
