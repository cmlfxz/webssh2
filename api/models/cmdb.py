from datetime import datetime
from sqlalchemy import UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer,String,Text,DateTime
import json

Base = declarative_base()

class Record(Base):
    __tablename__ = 'record'
    id = Column(Integer,primary_key=True)
    host = Column(String(100),comment='主机地址')
    username = Column(String(100),comment='登录主机')
    filename = Column(String(100),comment='文件名')
    download_url = Column(String(300), comment='文件下载地址')
    start_time =  Column(String(100),comment='起始时间')
    end_time =  Column(String(100),comment='结束时间')